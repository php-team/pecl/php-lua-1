php-lua-1 (1:1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0
  * Split the source package for PHP 5.6
  * Use package2.xml instead of package.xml

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 14:06:37 +0200

php-lua (2.0.7+1.1.0-14) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:42:24 +0100

php-lua (2.0.7+1.1.0-13) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:26 +0100

php-lua (2.0.7+1.1.0-12) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:48 +0100

php-lua (2.0.7+1.1.0-10) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:25:54 +0100

php-lua (2.0.7+1.1.0-9) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:00:56 +0100

php-lua (2.0.7+1.1.0-8) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:07:06 +0100

php-lua (2.0.7+1.1.0-7) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:07:56 +0100

php-lua (2.0.7+1.1.0-6) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:33 +0100

php-lua (2.0.7+1.1.0-5) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:50:46 +0100

php-lua (2.0.7+1.1.0-4) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:48 +0100

php-lua (2.0.7+1.1.0-3) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:40:08 +0100

php-lua (2.0.7+1.1.0-2) unstable; urgency=medium

  * Update for dh-php >= 2.0 support
  * Build the extension only for PHP 5 and PHP 7

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 06:20:02 +0200

php-lua (2.0.7+1.1.0-1) unstable; urgency=medium

  * New upstream version 2.0.7+1.1.0

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Mar 2020 10:53:52 +0100

php-lua (2.0.6+1.1.0+-2) unstable; urgency=medium

  * PHP 7.4 Rebuild

 -- Ondřej Surý <ondrej@debian.org>  Tue, 25 Feb 2020 09:31:04 +0100

php-lua (2.0.6+1.1.0+-1) unstable; urgency=medium

  * New upstream version 2.0.6+1.1.0+
  * Fixed the package-5.xml upstream file (package2.xml has to be used).

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 09:37:28 +0200

php-lua (2.0.6+1.1.0-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 2.0.6+1.1.0
  * Rebase patches for php-lua_2.0.6+1.1.0

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 08:46:02 +0200

php-lua (2.0.5+1.1.0-3) unstable; urgency=medium

  * Bump the dependency on dh-php to >= 0.33

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 10:11:38 +0000

php-lua (2.0.5+1.1.0-2) unstable; urgency=medium

  * Update maintainer email to team+php-pecl@tracker.debian.org

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 12:32:06 +0000

php-lua (2.0.5+1.1.0-1) unstable; urgency=medium

  * Update Vcs-* to salsa.d.o
  * New upstream version 2.0.5+1.1.0
  * Refresh patches

 -- Ondřej Surý <ondrej@debian.org>  Sun, 08 Apr 2018 06:29:18 +0000

php-lua (2.0.4+1.1.0+-1) unstable; urgency=medium

  * New upstream version 2.0.4+1.1.0+

 -- Ondřej Surý <ondrej@debian.org>  Mon, 31 Jul 2017 20:04:20 +0200

php-lua (2.0.4+1.1.0-1) unstable; urgency=medium

  * New upstream version 2.0.4+1.1.0

 -- Ondřej Surý <ondrej@debian.org>  Mon, 31 Jul 2017 19:09:44 +0200

php-lua (2.0.3+1.1.0-2) unstable; urgency=medium

  * Use DEB_HOST_MULTIARCH in config.m4

 -- Ondřej Surý <ondrej@debian.org>  Tue, 30 May 2017 12:06:14 +0200

php-lua (2.0.3+1.1.0-1) unstable; urgency=low

  * New upstream version 2.0.3+1.1.0
  * Initial release
  * Compile with Lua 5.2 (that's what's available on Ubuntu Trusty)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 30 May 2017 11:12:04 +0200
